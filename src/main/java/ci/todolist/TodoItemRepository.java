package ci.todolist;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class TodoItemRepository {

    @PersistenceContext(unitName = "datasource")
    private EntityManager entityManager;

    public List<TodoItem> getAll() {
        return entityManager.createQuery("from TodoItem", TodoItem.class).getResultList();
    }

    public TodoItem create(String name) {
        TodoItem todo = new TodoItem(name);
        entityManager.persist(todo);
        return todo;
    }

    public boolean isEmpty() {
        return count() == 0;
    }

    public Long count() {
        String countQuery = "select count(t) from TodoItem t";
        return entityManager.createQuery(countQuery, Long.class).getSingleResult();
    }

}
